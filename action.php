<?php
/** DokuWiki Plugin aCookieBanner (Action Component) @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @author larssc https://gitlab.com/larssc/
 */

// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();

/**Registers a callback function for a given event @param Doku_Event_Handler $controller DokuWiki's event controller object @return void*/
class action_plugin_acookiebanner extends DokuWiki_Action_Plugin {
/** [Custom event handler which performs action]
* @param Doku_Event $event  event object by reference
* @param mixed      $param  [the parameters passed as fifth argument to register_hook() when this handler was registered]
* @return void */
    public function register(Doku_Event_Handler $controller) {$controller->register_hook('TPL_ACT_RENDER', 'AFTER', $this, 'handle_tpl_act_render');}
        /* aCookieBanner plugin action */
        public function handle_tpl_act_render(Doku_Event &$event, $param) {
            if ($event->data != 'show') {return;}     
            if ($_COOKIE['DW_p_aCookieBanner'] == "1") {return;}

            /* SETUP LANG VARIABLES */
            $DWhash_regex='/^DW[0-9a-z]{32}$/';
            $position = hsc($this->getConf('position'));

            $show_cookies = hsc($this->getConf('show_cookies'));
            $show_details = hsc($this->getConf('show_details'));
            $show_details2 = hsc($this->getConf('show_details2'));
        
    	   $information = hsc($this->getConf('information'));
            if (!isset($information) || ($information == NULL)) {
                $information = hsc($this->getLang('information'));
            }
            // rebuild <a href="link">text</a> tags
            $information = str_replace("&lt;a href", '<a href', $information);
            $information = str_replace("&quot;&gt;", '">', $information);
            $information = str_replace("f=&quot;", 'f="', $information);
            $information = str_replace("&lt;/a&gt;", '</a>', $information);
            // allow html entities
            //$information = str_replace("&amp;", '&', $information);
            /* only allow relative pages
            $infreg='/(data|https?|ftps?|.*script)\:\/\/.*">/';
            $information = preg_replace($infreg, 'relative_links_only">', $information);*/
            
            $consent = hsc($this->getConf('consent'));
            if (!isset($consent) || ($consent == NULL)) {
                $consent = hsc($this->getLang('consent'));
            }
            // allow html entities
            //$consent = str_replace("&amp;", '&', $consent);
        
            $details = hsc($this->getConf('details'));
            if (!isset($details) || ($details == NULL)) {
                $details = hsc($this->getLang('details'));
            }
            $details_url = rawurldecode(hsc($this->getConf('details_url')));
            if (!isset($details_url) || ($details_url == NULL)) {
                $details_url = rawurldecode(hsc($this->getLang('details_url')));
                if(!preg_match('/^https?:\/\//i',$details_url)) $details_url = '';
            }
            $details2 = hsc($this->getConf('details2'));
            if (!isset($details2) || ($details2 == NULL)) {
                $details2 = hsc($this->getLang('details2'));
            }
            $details_url2 = rawurldecode(hsc($this->getConf('details_url2')));
            if (!isset($details_url2) || ($details_url2 == NULL)) {
                $details_url2 = rawurldecode(hsc($this->getLang('details_url2')));
                if(!preg_match('/^https?:\/\//i',$details_url2)) $details_url2 = ''; 
            }
            // set cookie strings
            if ($show_cookies == "1"){
                $show_cookie_active = hsc($this->getConf('show_cookie_active'));
                if (!isset($show_cookie_active) || ($show_cookie_active == NULL)) {
                    $show_cookie_active = hsc($this->getLang('show_cookie_active'));
                }
                $show_cookie_active_tooltip = hsc($this->getConf('show_cookie_active_tooltip'));
                if (!isset($show_cookie_active_tooltip) || ($show_cookie_active_tooltip == NULL)) {
                    $show_cookie_active_tooltip = hsc($this->getLang('show_cookie_active_tooltip'));
                }            
                $show_cookie_hidden = hsc($this->getConf('show_cookie_hidden'));
                if (!isset($show_cookie_hidden) || ($show_cookie_hidden == NULL)) {
                    $show_cookie_hidden = hsc($this->getLang('show_cookie_hidden'));
                }
                $show_cookie_hidden_tooltip = hsc($this->getConf('show_cookie_hidden_tooltip'));
                    if (!isset($show_cookie_hidden_tooltip) || ($show_cookie_hidden_tooltip == NULL)) {
                    $show_cookie_hidden_tooltip = hsc($this->getLang('show_cookie_hidden_tooltip'));
                }
                // set cookietypes
                $cookietype_necessary = hsc($this->getConf('cookietype_necessary'));
                if (!isset($cookietype_necessary) || ($cookietype_necessary == NULL)) {
                    $cookietype_necessary = hsc($this->getLang('cookietype_necessary'));
                }
                $cookietype_necessary2 = hsc($this->getConf('cookietype_necessary2'));
                if (!isset($cookietype_necessary2) || ($cookietype_necessary2 == NULL)) {
                    $cookietype_necessary2 = hsc($this->getLang('cookietype_necessary2'));
                }
                $cookietype_functional = hsc($this->getConf('cookietype_functional'));
                if (!isset($cookietype_functional) || ($cookietype_functional == NULL)) {
                    $cookietype_functional = hsc($this->getLang('cookietype_functional'));
                }                        
                $count_hash = 0;                       
                $dw_cookies=array();
                foreach ($_COOKIE as $key=>$val) {
                    if ($key == "DokuWiki") {$val="$cookietype_necessary";}
                    // only show extra notice if more than two cookies matching the regex for DW<hash>
                    if (preg_match("$DWhash_regex", $key)){
                        ++$count_hash;
                        if ($count_hash == 1){$val="$cookietype_necessary";}
                        if ($count_hash > 1){$val="$cookietype_necessary $cookietype_necessary2";}
                    }                
                    if ($key == "DOKU_PREFS") {$val="$cookietype_functional";}
                    if ($key == "DW_p_aCookieBanner") {$val="$cookietype_necessary";}
                    $dw_cookies[] = array($key=>$val);
                }
            }            
            /* DO OUTPUT */
            echo '<div class="p_acb-banner p_acb-' . $position . '">';
            echo "$information" . '<br/>';
        
            if ($show_details == "1") {
                echo '<a href="' . $details_url . '" target="_blank">' . "$details" . '</a>';
            }
            if ($show_details2 == "1") {
                if ($show_details == "1") {echo "&nbsp;";}
                echo '<a href="' . $details_url2 . '" target="_blank">' . "$details2" . '</a>';
            }
            // output all cookies
            if ($show_cookies == "1"){
                /* +/- from: https://stackoverflow.com/questions/59100312/single-button-to-show-hide-element-with-pure-css */
                if ($show_details == "1" || ($show_details2 == "1")) {echo "&nbsp;";}
                echo '<a id="p_acbhide1" href="#p_acbhide1" class="p_acbhide" title="' . "$show_cookie_active_tooltip" . '">' . "$show_cookie_active" . '</a>';
                echo '<a id="p_acbshow1" href="#p_acbshow1" class="p_acbshow" title="' . "$show_cookie_hidden_tooltip" . '">' . "$show_cookie_hidden" . '</a>';
                            
                echo '<div class="p_acbdetails">';
                echo "<ul>";            
                if ($show_details == "0" && ($show_details2 == "0")) {echo '<li><a href="' . $details_url2 . '" target="_blank">' . "$details2" . '</a></li>';}          
                // render cookies
                if (!isset($_COOKIE['DW_p_aCookieBanner'])) {echo "<li><i>+ DW_p_aCookieBanner &rarr; $cookietype_necessary</i></li>";}
                array_walk_recursive($dw_cookies, function ($val, $key) {echo "<li> $key &rarr; $val </li>";});
                echo "</ul>";               
                echo '</div>';
            }
            // show consent button and close our main div
            echo '&nbsp;<button id="p_acb_consent">' . "$consent" . '</button>';
            echo '</div>';
        }
}

// vim:ts=4:sw=4:et:
