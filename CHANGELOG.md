2020-09-06 v1.2
- Cookie information text can now fully be customized (use of links via **a tags** is allowed)
- Cookie information text now contains a link to a privacy page (privacy in your wiki root)
- De-activated alot of settings (you can re-activate them in: conf/metadata.php to show up in the admin panel or set them directly in conf/default.php)
- Added a slight transparency effect in style.css
- Fixed czech language I wrongly translated to slowak
- Improved language files and output

2020-09-04 v1.1.1
- Fixed: Issue #1 (language string not used in cookietype)

2020-09-02 v1.1
- DW_p_cookielaw2 is now a session cookie (ends when session ends)
- The Plugin now shows the cookies set by DokuWiki (Show Cookies)
- Show Details and Show Details2 can now be activated
- Show Details and Show Details2 have been deactivated and Show Cookies activated by default
- If Show Details and Show Details2 are deactivated Show Details2 will be shown in the cookielist (Show Cookies)
- Improved output

2020-08-28 v1.0
- Output can now be configured in the adminsettings (if not set or NULL, the language files are used)
- Cookie is now set to: strict and secure
- Cookie expires earlier (1 instead of 10 years)
- Wrote details2 for every language
- Included details2 and details_url2 in output of every language
- All generic cookie information links point to wikipedia
- Added space between Text and consent button
- Included spanish translation (wrong folder fix)
 
