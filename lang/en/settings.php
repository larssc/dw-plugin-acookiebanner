<?php
//@author larssc https://gitlab.com/larssc/

$lang['position']                       = 'Position of the cookiebanner.';
$lang['information']                    = 'Cookie notice (informative text).';
$lang['show_cookies']	                = 'Show session cookies set by DokuWiki';
$lang['show_details']	                = 'Show generic link (details)';
$lang['show_details2']	                = 'Show details link (details2)';
$lang['consent']                        = 'Consent button text';

$lang['details']                        = 'Title for link to generic information.';
$lang['details_url']                    = 'Link to generic information.';
$lang['details2']                       = 'Title for detailed information.';
$lang['details_url2']                   = 'Link to detailed information.';

$lang['show_cookie_active']             = '+ Text to show cookies (onclick)';
$lang['show_cookie_hidden']             = '- Text to hide cookies (onclick)';
$lang['show_cookie_active_tooltip']     = 'Tooltip for text to show cookies';
$lang['show_cookie_hidden_tooltip']     = 'Tooltip for text to hide cookies';

$lang['cookietype_necessary']           = 'Text to show for necessary cookies';
$lang['cookietype_necessary2']          = 'Text to show for necessary cookies with disclaimer that it may be another cookie type if used by a plugin';
$lang['cookietype_functional']          = 'Text to show for functional cookies';
$lang['cookietype_optional']            = 'Text to show for optional cookies';

//Setup VIM: ex: et ts=4 :
