<?php
//@author larssc https://gitlab.com/larssc/

$lang['privacy_site']                   = '<a href="doku.php?id=privacy">Privacy Policy</a>';
$lang['information']                    = 'This website uses cookies. By using our services, you agree with storing cookies on your computer. You also acknowledge that you have read and understood our ' . $lang['privacy_site'] . ". If you don't agree leave the website!";
$lang['consent']                        = 'OK';

$lang['show_cookie_active']             = '+ Show Cookies';
$lang['show_cookie_hidden']             = '- Show Cookies';
$lang['show_cookie_active_tooltip']     = 'Click to show cookies set by DokuWiki';
$lang['show_cookie_hidden_tooltip']     = 'Click to hide cookies set by DokuWiki';

$lang['cookietype_necessary']           = 'necessary';
$lang['cookietype_necessary2']          = ', if set by plugin maybe functional / optional';
$lang['cookietype_functional']          = 'functional';
$lang['cookietype_optional']            = 'optional';

$lang['details']                        = 'Information about cookies';
$lang['details_url']                    = 'https://en.wikipedia.org/wiki/HTTP_cookie';

$lang['details2']                       = 'Cookies used by Dokuwiki';
$lang['details_url2']                   = 'https://www.dokuwiki.org/faq:cookies';

//Setup VIM: ex: et ts=4 :
