<?php
//@author larssc https://gitlab.com/larssc/

$lang['position']                       = 'Posición del banner de cookies.';
$lang['information']                    = 'Aviso de cookies (texto informativo).';
$lang['show_cookies']	                = 'Mostrar cookies de sesión configuradas por DokuWiki';
$lang['show_details']	                = 'Mostrar enlace genérico (datails)';
$lang['show_details2']	                = 'Mostrar enlace de detalles (details2)';
$lang['consent']                        = 'Texto del botón de consentimiento';

$lang['details']                        = 'Título del enlace a información genérica.';
$lang['details_url']                    = 'Enlace a información genérica.';
$lang['details2']                       = 'Título para obtener información detallada.';
$lang['details_url2']	                = 'Enlace a información detallada.';

$lang['show_cookie_active']             = '+ Texto para mostrar cookies (onclick)';
$lang['show_cookie_hidden']             = '- Texto para ocultar cookies (onclick)';
$lang['show_cookie_active_tooltip']     = 'Información sobre herramientas para texto para mostrar cookies';
$lang['show_cookie_hidden_tooltip']     = 'Información sobre herramientas para texto para ocultar cookies';

$lang['cookietype_necessary']           = 'Texto para mostrar las cookies necesarias';
$lang['cookietype_necessary2']          = 'Texto para mostrar las cookies necesarias con la exención de responsabilidad de que puede ser otro tipo de cookie si lo usa un complemento';
$lang['cookietype_functional']          = 'Texto para mostrar cookies funcionales';
$lang['cookietype_optional']            = 'Texto para mostrar para cookies opcionales';

//Setup VIM: ex: et ts=4 :
