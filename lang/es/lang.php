<?php
//@author larssc https://gitlab.com/larssc/

$lang['privacy_site']                   = '<a href="doku.php?id=privacy">Página de privacidad</a>';
$lang['information']                    = 'Este sitio web utiliza cookies. Al utilizar el sitio web, acepta almacenar cookies en su computadora. También reconoce que ha leído y comprendido nuestro '. $lang['privacy_site'] . '. ¡Si no está de acuerdo, abandone el sitio web!';
$lang['consent']                        = 'OK';

$lang['show_cookie_active']             = '+ Mostrar cookies';
$lang['show_cookie_hidden']             = '- Mostrar cookies';
$lang['show_cookie_active_tooltip']     = 'Haga clic para mostrar las cookies establecidas por DokuWiki';
$lang['show_cookie_hidden_tooltip']     = 'Haga clic para ocultar las cookies establecidas por DokuWiki';

$lang['cookietype_necessary']           = 'necesaria';
$lang['cookietype_necessary2']          = ', si lo establece el complemento, puede ser funcional / opcional';
$lang['cookietype_functional']          = 'funcional';
$lang['cookietype_optional']            = 'optconal';

$lang['details']                        = 'Más información Cookies';
$lang['details_url']                    = 'https://es.wikipedia.org/wiki/Cookie_(inform%C3%A1tica)';

$lang['details2']                       = 'Más información Dokuwiki';
$lang['details_url2']                   = 'https://www.dokuwiki.org/faq:cookies';

//Setup VIM: ex: et ts=4 :
