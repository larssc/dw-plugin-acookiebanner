<?php
//@author larssc https://gitlab.com/larssc/

$lang['position']                       = 'Position des Cookiebanners.';
$lang['information']	                = 'Cookie Benachrichtigungstext (informativer Text).';
$lang['show_cookies']	                = 'Anzeigen der von DokuWiki gesetzten Cookies';
$lang['show_details']	                = 'Anzeigen der allgmeinen Informationen (details)';
$lang['show_details2']	                = 'Anzeigen der detaillierten Informationen (details2)';
$lang['consent']                        = 'Text des Zustimmungsbuttons';

$lang['details']                        = 'Titel des Links für allgemeine Cookie Informationen';
$lang['details_url']                    = 'Link zu den allgemeinen Cookie Informationen.';
$lang['details2']                       = 'Titel des Links für detaillierte Cookie Informationen';
$lang['details_url2']                   = 'Link zu den detaillierten Cookie Informationen.';

$lang['show_cookie_active']             = '+ Text zum anzeigen der Cookies (onclick)';
$lang['show_cookie_hidden']             = '- Text zum ausblenden Cookies (onclick)';
$lang['show_cookie_active_tooltip']     = 'Tooltip für den Text zum anzeigen der Cookies';
$lang['show_cookie_hidden_tooltip']     = 'Tooltip für den Text zum ausblenden der Cookies';

$lang['cookietype_necessary']           = 'Text zur Beschreibung der notwendigen Cookies';
$lang['cookietype_necessary2']          = 'Text zur Beschreibung der notwendigen Cookies, mit Hinweis auf funktionale/optionale Cookies';
$lang['cookietype_functional']          = 'Text zur Beschreibung von funktionalen Cookies';
$lang['cookietype_optional']            = 'Text zur Beschreibung von optionalen Cookies';

//Setup VIM: ex: et ts=4 :
