<?php
//@author larssc https://gitlab.com/larssc/
 
$lang['privacy_site']                   = '<a href="doku.php?id=privacy">Datenschutzerklärung</a>';
$lang['information']                    = 'Diese Website verwendet Cookies. Mit der Nutzung der Website erklärst du dich einverstanden, dass Cookies auf deinem Computer gespeichert werden. Außerdem bestätigst du, dass du unsere ' . $lang['privacy_site'] . ' gelesen und verstanden hast. Ansonsten, verlasse diese Website!';
$lang['consent']                        = 'OK';

$lang['show_cookie_active']             = '+ Cookies anzeigen';
$lang['show_cookie_hidden']             = '- Cookies ausblenden';
$lang['show_cookie_active_tooltip']     = 'Drücke die Linke Maustaste um die von DokuWiki gesetzten Cookies anzuzeigen';
$lang['show_cookie_hidden_tooltip']     = 'Drücke die Linke Maustaste um die von DokuWiki gesetzten Cookies auszublenden';

$lang['cookietype_necessary']           = 'notwendig';
$lang['cookietype_necessary2']          = ', Plugin Cookies evtl. funktional / optional';
$lang['cookietype_functional']          = 'funktional';
$lang['cookietype_optional']            = 'optional';

$lang['details']                        = 'Information über Cookies';
$lang['details_url']                    = 'https://de.wikipedia.org/wiki/Cookie';

$lang['details2']                       = 'Cookies in Dokuwiki';
$lang['details_url2']                   = 'https://www.dokuwiki.org/faq:cookies';

//Setup VIM: ex: et ts=4 :
