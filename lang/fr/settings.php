<?php
//@author larssc https://gitlab.com/larssc/

$lang['position']                       = 'Position du cookiebanner.';
$lang['information']                    = 'Avis sur les cookies (texte informatif).';
$lang['show_cookies']	                = 'Afficher les cookies de session définis par DokuWiki';
$lang['show_details']	                = 'Afficher le lien générique (details)';
$lang['show_details2']	                = 'Afficher le lien des détails (details2)';
$lang['consent']                        = 'Texte du bouton de consentement';

$lang['details']                        = 'Titre du lien vers des informations génériques.';
$lang['details_url']                    = 'Lien vers des informations génériques.';
$lang['details2']                       = 'Titre pour des informations détaillées.';
$lang['details_url2']                   = 'Lien vers des informations détaillées.';

$lang['show_cookie_active']             = '+ Texte pour afficher les cookies (onclick)';
$lang['show_cookie_hidden']             = '- Texte pour masquer les cookies (onclick)';
$lang['show_cookie_active_tooltip']     = 'Info-bulle pour le texte pour afficher les cookies';
$lang['show_cookie_hidden_tooltip']     = 'Info-bulle pour le texte pour masquer les cookies';

$lang['cookietype_necessary']           = 'Texte à afficher pour les cookies nécessaires';
$lang['cookietype_necessary2']          = "Texte à afficher pour les cookies nécessaires avec clause de non-responsabilité indiquant qu'il" .
                                          " peut s'agir d'un autre type de cookie s'il est utilisé par un plugin";
$lang['cookietype_functional']          = 'Texte à afficher pour les cookies fonctionnels';
$lang['cookietype_optional']            = 'Texte à afficher pour les cookies facultatifs';

//Setup VIM: ex: et ts=4 :
