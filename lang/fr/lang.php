<?php
//@author larssc https://gitlab.com/larssc/

$lang['privacy_site']                   = '<a href="doku.php?id=privacy">Politique de confidentialité</a>';
$lang['information']                    = 'Ce site web utilise des cookies. En utilisant le site Web, vous acceptez le stockage de cookies sur votre ordinateur. Vous reconnaissez également avoir lu et compris notre ' . $lang ['privacy_site'] . ". Si vous n'êtes pas d'accord, quittez le site Web!";
$lang['consent']                        = 'OK';

$lang['show_cookie_active']             = '+ Afficher les cookies';
$lang['show_cookie_hidden']             = '- Afficher les cookies';
$lang['show_cookie_active_tooltip']     = 'Cliquez pour afficher les cookies définis par DokuWiki';
$lang['show_cookie_hidden_tooltip']     = 'Cliquez pour masquer les cookies définis par DokuWiki';

$lang['cookietype_necessary']           = 'nécessaire';
$lang['cookietype_necessary2']          = ', si défini par le plugin peut-être fonctionnel / facultatif';
$lang['cookietype_functional']          = 'fonctionnel';
$lang['cookietype_optional']            = 'facultatif';

$lang['details']                        = 'En savoir plus Cookies';
$lang['details_url']                    = 'https://fr.wikipedia.org/wiki/Cookie_(informatique)';

$lang['details2']                       = 'En savoir plus Dokuwiki';
$lang['details_url2']                   = 'https://www.dokuwiki.org/faq:cookies';

//Setup VIM: ex: et ts=4 :
