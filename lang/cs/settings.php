<?php
//@author larssc https://gitlab.com/larssc/

$lang['position']                       = 'Pozice banneru cookie.';
$lang['information']                    = 'Upozornění na soubory cookie (informativní text).';
$lang['show_cookies']	                = 'Zobrazit soubory cookie relace nastavené DokuWiki';
$lang['show_details']	                = 'Zobrazit obecný odkaz (details)';
$lang['show_details2']	                = 'Zobrazit odkaz na podrobnosti (details2)';
$lang['consent']                        = 'Text tlačítka souhlasu';

$lang['details']                        = 'Název odkazu na obecné informace.';
$lang['details_url']                    = 'Odkaz na obecné informace.';
$lang['details2']                       = 'Název pro podrobné informace.';
$lang['details_url2']	                = 'Odkaz na podrobné informace.';

$lang['show_cookie_active']             = '+ Text pro zobrazení cookies (onclick)';
$lang['show_cookie_hidden']             = '- Text pro skrytí cookies (onclick)';
$lang['show_cookie_active_tooltip']     = 'Popisek pro text zobrazující soubory cookie';
$lang['show_cookie_hidden_tooltip']     = 'Popisek skrytí text zobrazující soubory cookie';

$lang['cookietype_necessary']           = 'Text pro zobrazení nezbytných cookies';
$lang['cookietype_necessary2']          = 'Text, který ukazuje potřebné soubory cookie s vyloučením odpovědnosti, že to může být jiný typ souborů cookie, pokud je používán zásuvným modulem';
$lang['cookietype_functional']          = 'Text, který se zobrazí pro funkční soubory cookie.';
$lang['cookietype_optional']            = 'Text, který se zobrazí pro volitelné soubory cookie';

//Setup VIM: ex: et ts=4 :
