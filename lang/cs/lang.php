<?php
//@author larssc https://gitlab.com/larssc/

$lang['privacy_site']                   = '<a href="doku.php?id=privacy">Stránka ochrany osobních údajů</a>';
$lang['information']                    = 'Tento web používá cookies. Používáním webových stránek souhlasíte s ukládáním cookies do vašeho počítače. Rovněž berete na vědomí, že jste si přečetli a rozumíte našemu '. $lang['privacy_site'] . ' Pokud nesouhlasíte, opusťte web!';
$lang['consent']                        = 'OK';

$lang['show_cookie_active']             = '+ Zobrazit soubory cookie';
$lang['show_cookie_hidden']             = '- Zobrazit soubory cookie';
$lang['show_cookie_active_tooltip']     = 'Kliknutím zobrazíte soubory cookie nastavené DokuWiki';
$lang['show_cookie_hidden_tooltip']     = 'Kliknutím skryjete soubory cookie nastavené DokuWiki';

$lang['cookietype_necessary']           = 'nutné';
$lang['cookietype_necessary2']          = ', pokud je nastaven pluginem možná funkční / volitelný';
$lang['cookietype_functional']          = 'funkční';
$lang['cookietype_optional']            = 'volitelný';

$lang['details']                        = 'Informace o cookies';
$lang['details_url']                    = 'https://cs.wikipedia.org/wiki/HTTP_cookie';

$lang['details2']                       = 'Soubory cookie používané Dokuwiki';
$lang['details_url2']                   = 'https://www.dokuwiki.org/faq:cookies';

//Setup VIM: ex: et ts=4 :
