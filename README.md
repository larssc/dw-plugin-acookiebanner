**aCookieBanner Plugin for DokuWiki**

The Advanced Cookiebanner Plugin (aCookieBanner) for DokuWiki inserts a configurable banner at the top or bottom of the website asking about the user's consent to store cookies on the used computer.

It has been forked from:
[cookielaw](https://www.dokuwiki.org/plugin:cookielaw) by [Michal Koutny](https://github.com/fykosak/dokuwiki-plugin-cookielaw/)

This Plugin aims to provide a better base to comply to the GDPR/DSGVO, *however* the writers of this plugin will not take responsibility for legal correctness of any sort.
For now this Plugin assumes only its own cookie and the DokuWiki cookies are set.      

**New features**
- All Output can now be configured in the adminpanel (many options are disabled in conf/metadata.php)
- All cookies can be displayed
- Contains a link to a privacy page in your wiki pageroot (data/pages/privacy.txt)
- The used cookie for the cookiebanner now is a session cookie
- Slight transparency effect
- Improved language files and output

**Weblinks**
- [Download](https://gitlab.com/larssc/dw-plugin-acookiebanner/-/archive/master/master.zip)
- [Docs](https://gitlab.com/larssc/dw-plugin-acookiebanner)
- [Bugs](https://gitlab.com/larssc/dw-plugin-acookiebanner/-/issues)

**Installation**

Use the extension manager to install the plugin, should you not know how to do that, please refer to http://www.dokuwiki.org/plugins for additional info on how to install plugins in DokuWiki. *If you install this plugin manually*, make sure it is installed in **./lib/plugins/acookiebanner** or else it won't work!

----

**Copyright (C) GNU General Public License 2**

**Credits**
- @author larssc
- Michal Koutny, Django, Doc, Niko P. (git history left intact)
