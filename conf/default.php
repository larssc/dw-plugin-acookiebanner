<?php
// @author larssc <6862416-larssc@users.noreply.gitlab.com>

$conf['position']                       = 'bottom';
$conf['show_cookies']                   = '1';
$conf['show_details']                   = '0';
$conf['show_details2']                  = '0';
$conf['information']                    = '';
$conf['consent']                        = '';
$conf['details']                        = '';
$conf['details_url']                    = '';
$conf['details2']                       = '';
$conf['details_url2']                   = '';
$conf['show_cookie_active']             = '';
$conf['show_cookie_hidden']             = '';
$conf['show_cookie_active_tooltip']     = '';
$conf['show_cookie_hidden_tooltip']     = '';
$conf['cookietype_necessary']           = '';
$conf['cookietype_necessary2']          = '';
$conf['cookietype_functional']          = '';
$conf['cookietype_optional']            = '';

// vim:ts=4:sw=4:et:
