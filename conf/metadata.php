<?php
/** Options for the cookielaw2 plugin
 * @author larssc <6862416-larssc@users.noreply.gitlab.com> */

$meta['position']                       = array('multichoice', '_choices' => array('top', 'bottom'));
$meta['information']                    = array('string');
$meta['details2']                       = array('string');
$meta['details_url2']                   = array('string');

$meta['show_cookie_active']             = array('string');
$meta['show_cookie_hidden']             = array('string');

$meta['consent']                        = array('string');

//$meta['show_cookies']                   = array('onoff');
//$meta['show_details']                   = array('onoff');
//$meta['show_details2']                  = array('onoff');
//$meta['show_cookie_active_tooltip']     = array('string');
//$meta['show_cookie_hidden_tooltip']     = array('string');

//$meta['cookietype_necessary']           = array('string');
//$meta['cookietype_necessary2']          = array('string');
//$meta['cookietype_functional']          = array('string');
//$meta['cookietype_optional']            = array('string');

//$meta['details']                        = array('string');
//$meta['details_url']                    = array('string');

// vim:ts=4:sw=4:et:
